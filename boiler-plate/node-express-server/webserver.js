const express = require('express');
const app = express();
const port = 3000;
// user-agents DOES NOT READ the actual userAgent string.
// it just generates random userAgent strings.
let userAgent = require('user-agents');
const dir = '/user';

app.get('/', (req, res) => {
  res.send('Hello Express!');
  console.log(`Received GET request to the API with userAgent:`);
  console.log(userAgent.toString());
});

app.post('/', (req, res) => {
  res.send('Received a POST request to the API...');
  console.log(`Received POST request from port: ${port} to ${dir}userAgent:`);
  console.log(userAgent.toString());
});

app.put('/user', (req, res) => {
  res.send('Received a PUT request to the API...');
  console.log(`Received PUT request from port: ${port} to ${dir}userAgent:`);
  console.log(userAgent.toString());
});

app.delete('/user', (req, res) => {
  res.send('Received a DELETE request to the API...');
  console.log(`Received DELETE request from port: ${port} to ${dir}userAgent:`);
  console.log(userAgent.toString());
});

app.listen(port, () => {
  console.log(`Listening on port ${port}`);
});

app.use(() => {
  console.log(userAgent);
});

// userAgentLogging
userAgent = new userAgent(); // eslint new-cap: "off"
console.log(userAgent.toString()); // eslint new-cap: "off"
