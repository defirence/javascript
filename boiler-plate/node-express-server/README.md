# node-express-server
A simple node.js webserver with basic API functionality, written in pure JavaScript.
Dependencies:
- yarn version: 1.22.19
- npm version: 8.15.0
- node.js version: v16.17.0 LTS
- ECMA version: `latest`, other information present in `.eslintrc.json`

Makes use of Snyk for IaC and Security scanning.