# javascript
[![eslint](https://github.com/Defirence/javascript/actions/workflows/eslint.yml/badge.svg?branch=main)](https://github.com/Defirence/javascript/actions/workflows/eslint.yml)

A collection of JavaScript boilerplate templates, some basic configurations and hello-world projects as a portfolio.

<p xmlns:dct="http://purl.org/dc/terms/" xmlns:vcard="http://www.w3.org/2001/vcard-rdf/3.0#">
  <a rel="license"
     href="https://creativecommons.org/publicdomain/zero/1.0/">
    <img src="https://licensebuttons.net/p/zero/1.0/80x15.png" style="border-style: none;" alt="CC0" />
  </a>
  <br />
  To the extent possible under law,
  <a rel="dct:publisher"
     href="https://www.github.com/Defirence/javascript">
    <span property="dct:title">Defirence</span></a>
  has waived all copyright and related or neighboring rights to
  <span property="dct:title">javascript</span>.
This work is published from:
<span property="vcard:Country" datatype="dct:ISO3166"
      content="ZA" about="https://www.github.com/Defirence/javascript">
  South Africa</span>.
</p>

### Project Maintenance Support:

Product subscriptions provided for free under the JetBrains OSS Program, which includes but is not limited to WebStorm.

<img src="https://resources.jetbrains.com/storage/products/company/brand/logos/WebStorm_icon.png" width="100" height="100" alt="webstorm icon"><img src="https://resources.jetbrains.com/storage/products/company/brand/logos/jb_square.png" width="100" height="100" alt="jetbrains logo">
